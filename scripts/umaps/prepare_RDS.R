##Script to manually remove genes now in a list of custom markers, remove cell lines unnecessary for the visualization and filter out elements with excessive mitochondrial fractions (>0.35)

library(Seurat)
  library(plyr)
  library(dplyr)
  library(SingleCellExperiment)
  library(optparse)
  library(reticulate)
  library(ggplot2)
  library(reshape2)
  library(cowplot)
  library(Matrix)
  library(patchwork)
  library(tidyverse)
seurat_integrated <- readRDS("./solid.integrated_seurat_object.RDS")


newgenes<-readLines("custom_marker_genes.txt")
newgenes_filtered <- newgenes[which(newgenes %in% rownames(seurat_integrated))]

seurat_integrated <- RunUMAP(seurat_integrated, nn.name = "weighted.nn", reduction.name = "wnn.umap", reduction.key = "wnnUMAP_", features = newgenes_filtered, min.dist = 0.1)

seurat_integrated <- subset(x = seurat_integrated, subset = celltype_major != "Dendritic.cells")

seurat_integrated_mt035 <- subset(x = seurat_integrated, subset = fractionMT < 0.35)

seurat_integrated@meta.data$celltype_major <- droplevels(seurat_integrated@meta.data$celltype_major)

saveRDS(seurat_integrated, "solid.integrated_seurat_object_customGenes_minDist.RDS")
saveRDS(seurat_integrated_mt035, "solid.integrated_seurat_object_customGenes_minDist_mt035.RDS")

