#################################################
## File name: integrateSamples_analyseCohort_ADT_GEX.R
## Author: Linda Grob
## Based on: integrateSamples_analyseCohort.R
## Date created: September 2020
## R Version: 3.5.1
##################################################

## GENERAL:
## This script is used for single cell integration analyses having ADT and GEX data using the Seurat R package.


## INPUT:
## - script takes list of RDS files (that contain SCE objects containing results of an
##   scRNA-seq experiment) and integrates those samples into one integrated Seurat object
##   (using the R package Seurat).
## - script takes second list of RDS files that contain results of GSVA analyses of
##   the same samples. IMPORTANT: Both lists have to be in the same order.
## - script can optionally work with an additional table of meta data about the samples.
## --> with package optparse for parsing command line arguments no parameter is strictly needed. If there is no meta data table to add simply do not use --add_meta
## For more details regarding input see help phrases of command line arguments.

##   META DATA FILE:
## Example:
##   sampleID     | cancerStage | pctRibosomalReads | reference
##   ----------------------------------------------------------
##   sample_1.RDS | stage III   | 0.3               | reference
##   sample_2.RDS | stage IV    | 0.1               | other
## If a plot should be generated from a meta data column the respective code block can be added in the Sample Visualisation section of the script.
## The Seurat function DimPlot() is used for categorical data and the function FeaturePlot() for continuous data.
## Note that DimPlot() does not plot a legend title using default settings (see additional line in existing code).

## OUTPUT:
## - integrated Seurat object in an RDS file.
## - UMAP plots showing GSVA results
## - UMAP plots characterising the cohort of samples. The plots include all integrated cells in one UMAP
##   and visualise data like cell types or g2m_score, n_umi, fractionMT.
## - UMAP plots showing gene expression
## - mean expression table per seurat cluster
## - UMAP showing combined adt & gex expression

## REMARK sampleID:
## The default is that the whole file name is entered as the sampleID (automatically retrieved from the input files and entered by the user in the meta data table).
## This is to keep this script very general and functioning without initial changes. But, especially for the plot
## showing the UMAP with all cells coloured by sampleID it is not optimal. If a shorter sampleID is desired you find example code with
## regular expressions to get a substring of the file name and have it serve as the 'sampleID'. This regular expression has to be changed and activated in two places
## indicated with 1) and 2).

##Example Call
#Rscript umaps.R --cohort_object ./solid.integrated_seurat_object_customGenes_minDist_mt035.RDS --outName solid_customGenes_minDist_mt035 --outdir ./mt035/ --colour_config colour_config.merged_newset.txt --add_meta=metadata_table.txt --lookup reference_lookup.tab --thresholds thresholds.txt

suppressPackageStartupMessages({
  library(Seurat)
  library(plyr)
  library(dplyr)
  library(SingleCellExperiment)
  library(optparse)
  library(reticulate)
  library(ggplot2)
  library(reshape2)
  library(cowplot)
  library(Matrix)
  library(patchwork)
  library(tidyverse)
})
# parse command line arguments
 option_list <- list(
   make_option("--cohort_object", type = "character", help = "Integrated Seurat Object"),
   make_option("--colour_config", type = "character", help = "Path to config file table that sorts colours to cell types"),
   make_option("--add_meta", type = "character", help = "Tab delimited text file with additional meta data. The first column must have header 'sampleID'. For a default run the full file name of the respective SCE analysis object is required, refer to script documentation on different sampleID formatting. Column headers describe categories. One line per input sample is required. Meta data file itself is optional: without table simply do not use --add_meta"),
   make_option("--lookup", type = "character", help = "Full path to a lookup table matching protein to antibody names."),
   make_option("--thresholds", type = "character", help = "Full path to a table listing adt thresholds for every sample included in the annalysis."),
   make_option("--outdir", type = "character", help = "Full path to output directory."),
   make_option("--outName", type = "character", help = "Prefix name of output files.")
 )
 opt_parser <- OptionParser(option_list = option_list)
 opt <- parse_args(opt_parser)


# convenience function for string concatenation
"%&%" <- function(a, b) paste(a, b, sep = "")
empty_string <- ""


###################################
###   Set plotting Parameters   ###
###################################

# If one of the values plot_aspect_ratio, chunksize, or number of columns is changed in the following section
# the other values should be checked as well.
# Also, check physical size of the generated output file in the respective plotting section of the script.

# IMPORTANT: when generating UMAP plots with the function RunUMAP() from Seurat the parameters "spread", "min.dist" and "n.neighbors"
# have a great impact on the resulting plot. They should be checked and optimised for each project.

# for all UMAP plots the same aspect ratio is used to have a comparable visualisation
plot_aspect_ratio <- 0.7

### The selected genes and gene sets are plotted in chunks so that the file size is not too large:

# chunksize_genes determines the number of genes shown in one expression plot
chunksize_genes <- 9
# columns_genes determines the number of columns the gene expression plots are arranged in.
## E.g. "columns_genes <- 1" arranges all plots in the output file below one another in a single column.
columns_genes <- 3

# chunk size of gene sets in GSVA plots
chunksize_genesets <- 12
# number of columns in GSVA plots
columns_genesets <- 4

# override the theme change of cowplot
theme_set(theme_grey())
print(Sys.time())


########################
###   Read in data   ###
########################

# read in color config
#config <- read.csv("colour_config.merged.txt", sep = "\t", stringsAsFactors = FALSE)
config <- read.csv(opt$colour_config, sep = "\t", stringsAsFactors = FALSE)
print(config)

# read in seurat object
#seurat_integrated <- readRDS("Cohort.integrated_seurat_object.RDS")
seurat_integrated <- readRDS(opt$cohort_object)
print(seurat_integrated)
cell_attributes <- as.data.frame(Embeddings(seurat_integrated, reduction = "wnn.umap"))
print(cell_attributes)
print(rownames(cell_attributes))
print(substring(rownames(cell_attributes),0,16))
print(unique(substring(rownames(cell_attributes),0,16)))
print(length(rownames(cell_attributes)))
print(length(unique(substring(rownames(cell_attributes),0,16))))
print(duplicated(substring(rownames(cell_attributes),0,16)))
cell_attributes <- cell_attributes[!duplicated(substring(rownames(cell_attributes),0,16)), ]
print(cell_attributes)
rownames(cell_attributes) <- substring(rownames(cell_attributes),0,16)
print(head(cell_attributes))


metadata <- seurat_integrated@meta.data
metadata <- metadata[!duplicated(substring(rownames(metadata),0,16)),]
print(head(rownames(metadata)))
rownames(metadata) <- substring(rownames(metadata),0,16)
print(head(rownames(metadata)))
print(colnames(metadata))
metadata$barcodes <- NULL
metadata$barcodes <- rownames(metadata)
print(colnames(metadata))
print(metadata$barcodes)
print(head(metadata))
cell_attributes$barcodes <- NULL
cell_attributes$barcodes <- rownames(cell_attributes)
cell_attributes <- plyr::join(cell_attributes, metadata, by="barcodes")
rownames(cell_attributes) <- cell_attributes$barcodes
cell_attributes$Row.names <- NULL
print(head(cell_attributes))



print(head(metadata))
number_samples <- length(unique(metadata$sampleID))
# optionally, read in an additional meta data table
print(empty_string)
 if (is.character(opt$add_meta)) {
   addMeta_table <- read.csv(opt$add_meta, sep = "\t", stringsAsFactors = FALSE)
   ## Use sampleID up to (but excluding) the second "." as shorter sampleID
   addMeta_table$sampleID <- regmatches(addMeta_table$sampleID, regexpr("[^\\.]+[^\\.]+", addMeta_table$sampleID))
   print("Reading in meta data table: check if input samples and meta data sampleIDs are identical")
   print(addMeta_table$sampleID)
   stopifnot(sort(addMeta_table$sampleID) == sort(unique(metadata$sampleID)))
 } else if (is.null(opt$add_meta)) {
   print("No input file with additional meta data given.")
   addMeta_table <- NULL
 } else {
   print("Something went wrong reading in the meta data table.")
 }


# Read in lookup table

lookup <-read.csv(opt$lookup, sep = "\t", stringsAsFactors = FALSE)
lookup$Antibody <- sub("_", "-", lookup$Antibody)
print(lookup)
# Read in thresholds
thresholds <- read.csv(opt$thresholds, sep= "\t", stringsAsFactors = FALSE)
thresholds$Antibody<- sub("_", "-", thresholds$Antibody)
rownames(thresholds) <- toupper(thresholds$Antibody)
print(tail(thresholds,20))

set.seed(2)

config <- read.csv(opt$colour_config, sep = "\t", stringsAsFactors = FALSE)

config$class <- NULL
config$indication <- NULL
# have character vector of colour names from the colour config

config <- rbind(config, c("uncertain", "grey50", "uncertain"))
config <- rbind(config, c("unknown", "black", "unknown"))
# give names to colours from other column of colour config
ct.color <- config$colour
names(ct.color) <- config$mapping
print(ct.color)

# make sure "original objects" contain the correct mapping for the celltypes

# get integer vector containing the index of the matching colour in ct.color for each
# level of seurat_integrated[[]]$celltype_major
id.first.ct <- match(levels(seurat_integrated[[]]$celltype_major), names(ct.color))
id.first.ct
id.final.ct <- match(levels(seurat_integrated[[]]$celltype_final), names(ct.color))
id.final.ct

###   Plot general cohort composition   ###
print(empty_string)
print("Plot general cohort composition.")
print(Sys.time())

test=seurat_integrated
levels(test@meta.data$celltype_major) <- c(levels(test@meta.data$celltype_major), "NK.cells", "Macrophages", "Fibroblasts", "Melanoma", "Keratinocytes", "Pericytes")
test@meta.data$celltype_major[grep("NK",test@meta.data$celltype_final)] <- "NK.cells"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major[grep("Macrophages",test@meta.data$celltype_final)] <- "Macrophages"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major[grep("Fibroblasts",test@meta.data$celltype_final)] <- "Fibroblasts"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major[grep("Melanoma",test@meta.data$celltype_final)] <- "Melanoma"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major[grep("Keratinocytes",test@meta.data$celltype_final)] <- "Keratinocytes"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major[grep("Pericyte",test@meta.data$celltype_final)] <- "Pericytes"#grep("NK",test@meta.data$celltype_final,value=TRUE)
test@meta.data$celltype_major <- droplevels(test@meta.data$celltype_major)


id.first.ct <- match(levels(test[[]]$celltype_major), names(ct.color))

mycelltypes = c("B.cells", "CAFs", "Dendritic.cells.Plasmacytoid", "Endothelial.cells", "Fibroblasts", "Keratinocytes", "Macrophages", "Melanocytes", "Melanoma", "Neural.cells", "NK.cells", "Pericytes", "Plasma.cells", "Secretory.epithelial.cells", "Smooth.muscle.cells", "T.cells", "uncertain", "unknown")
mylabels = c("B.cells", "CAFs", "Dendritic.cells.Plasmacytoid", "Endothelial.cells", "Fibroblasts", "Keratinocytes", "Macrophages", "Melanocytes", "Melanoma", "Neural.cells", "NK.cells", "Pericytes", "Plasma.cells", "Secretory.epithelial.cells", "Smooth.muscle.cells", "T.cells", "uncertain", "unknown")
mycolors = c("green4", "chocolate1", "darkolivegreen1", "magenta", "chocolate4", "magenta4", "wheat3", "red2", "red4", "rosybrown", "violetred1", "burlywood1", "darkolivegreen", "plum2", "darkslategrey", "blue2", "grey50", "black")
names(mycolors)<-mylabels
###
p3 <- DimPlot(test, reduction = 'wnn.umap', label = FALSE,group.by = "celltype_major",cols = ct.color[id.first.ct],  repel = TRUE, label.size = 2.5) +
  theme(aspect.ratio = plot_aspect_ratio) +
  guides(color = guide_legend(title = "celltype_major", override.aes = list(size = 3)))
#p3 
p3a<-p3+scale_color_manual(name="celltype_final", breaks=mycelltypes,label=mylabels, values=mycolors)
p3a

ggsave(filename = paste0(opt$outdir, opt$outName, ".sample_integration_celltypes_major.png"),
       width = 34, height = 20, dpi = 300, units = "cm", bg="white")

print("Written plot 3")

