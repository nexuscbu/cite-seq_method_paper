samplePrimaryMelanomaIndex=read.table("/cluster/project/nexus/phrt/levesque_melanoma_2019/data/20191016/indexHoppingADT/primaryMelanoma_pooled/barcodes.tsv")

samplePrimaryMelanomaCellRanger=read.table("/cluster/project/nexus/phrt/levesque_melanoma_2019/data/20191016/primaryMelanoma/analysis_pooledSample/CellRangerADT/primaryMelanoma_pooled/outs/filtered_feature_bc_matrix/barcodes.tsv")



head(samplePrimaryMelanomaIndex)
head(samplePrimaryMelanomaCellRanger)
samplePrimaryMelanomaCellRanger$V1 <- substr(samplePrimaryMelanomaCellRanger$V1, 1, 16)

intersection <- intersect(samplePrimaryMelanomaIndex$V1,samplePrimaryMelanomaCellRanger$V1)

write.table(intersection, file ="intersect_cellranger.indexhoppingremoval.barcodes.txt",row.names=FALSE,quote=FALSE,col.names=FALSE)
#print(length(setdiff(sample5000$V1,sample8000$V1)))
#print(length(setdiff(sample8000$V1,sample5000$V1)))

