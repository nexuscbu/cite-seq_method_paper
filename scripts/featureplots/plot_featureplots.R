## Script to create the feature plots

##Example calls
##Liquid
##Rscript plot_featureplots.R --cohort_object /cluster/project/nexus/phrt/levesque_melanoma_2019/cohort_integration/PBMC_untreated/analysis/PBMC_untreated.integrated_seurat_object.RDS --outName PBMC_untreated --outdir /cluster/project/nexus/phrt/levesque_melanoma_2019/cohort_integration/PBMC_untreated/analysis/ --colour_config /cluster/project/nexus/phrt/levesque_melanoma_2019/cohort_integration/colour_config.merged.txt --add_meta liquid_umap_newlegend/metadata_table.txt --lookup /cluster/project/nexus/phrt/levesque_melanoma_2019/cohort_integration/reference_lookup.tab --thresholds liquid_umap_newlegend/thresholds.txt
#Solid
##Rscript plot_featureplots.R --cohort_object solid.integrated_seurat_object_customGenes_minDist_mt035.RDS --outName solid --outdir ./ --colour_config colour_config.merged_newset.txt --add_meta metadata_table.txt --lookup reference_lookup.tab --thresholds thresholds.txt 


  library(Seurat)
  library(plyr)
  library(dplyr)
  library(SingleCellExperiment)
  library(optparse)
  library(reticulate)
  library(ggplot2)
  library(reshape2)
  library(cowplot)
  library(Matrix)
  library(patchwork)
  library(tidyverse)

option_list = list(
make_option("--cohort_object", type = "character", default = "/cluster/project/nexus/matteo/levesque_melanoma_2019_phrt_extended/1-umap/new_col_config/analysis/mindist_change/filtering/solid.integrated_seurat_object_customGenes_minDist_mt035.RDS"),
make_option("--outName", type = "character", default = "solid"),
make_option("--outdir", type = "character", default = "./"),
make_option("--colour_config", type = "character", default = "/cluster/project/nexus/matteo/levesque_melanoma_2019_phrt_extended/1-umap/new_col_config/analysis/mindist_change/filtering/colour_config.merged_newset.txt"),
make_option("--add_meta", type = "character", default = "/cluster/project/nexus/matteo/levesque_melanoma_2019_phrt_extended/metadata_table.txt"),
make_option("--lookup", type = "character", default = "/cluster/project/nexus/matteo/levesque_melanoma_2019_phrt_extended/reference_lookup.tab"),
make_option("--thresholds", type = "character", default = "/cluster/project/nexus/matteo/levesque_melanoma_2019_phrt_extended/thresholds.txt")
)
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

# convenience function for string concatenation
"%&%" <- function(a, b) paste(a, b, sep = "")
empty_string <- ""


###################################
###   Set plotting Parameters   ###
###################################

# If one of the values plot_aspect_ratio, chunksize, or number of columns is changed in the following section
# the other values should be checked as well.
# Also, check physical size of the generated output file in the respective plotting section of the script.

# IMPORTANT: when generating UMAP plots with the function RunUMAP() from Seurat the parameters "spread", "min.dist" and "n.neighbors"
# have a great impact on the resulting plot. They should be checked and optimised for each project.

# for all UMAP plots the same aspect ratio is used to have a comparable visualisation
plot_aspect_ratio <- 0.7

### The selected genes and gene sets are plotted in chunks so that the file size is not too large:

# chunksize_genes determines the number of genes shown in one expression plot
chunksize_genes <- 9
# columns_genes determines the number of columns the gene expression plots are arranged in.
## E.g. "columns_genes <- 1" arranges all plots in the output file below one another in a single column.
columns_genes <- 3

# chunk size of gene sets in GSVA plots
chunksize_genesets <- 12
# number of columns in GSVA plots
columns_genesets <- 4

# override the theme change of cowplot
theme_set(theme_grey())
print(Sys.time())


########################
###   Read in data   ###
########################

# read in color config
#config <- read.csv("colour_config.merged.txt", sep = "\t", stringsAsFactors = FALSE)
config <- read.csv(opt$colour_config, sep = "\t", stringsAsFactors = FALSE)
print(config)

# read in seurat object
#seurat_integrated <- readRDS("Cohort.integrated_seurat_object.RDS")
seurat_integrated <- readRDS(opt$cohort_object)
print(seurat_integrated)
cell_attributes <- as.data.frame(Embeddings(seurat_integrated, reduction = "wnn.umap"))
print(cell_attributes)
print(rownames(cell_attributes))
print(substring(rownames(cell_attributes),0,16))
print(unique(substring(rownames(cell_attributes),0,16)))
print(length(rownames(cell_attributes)))
print(length(unique(substring(rownames(cell_attributes),0,16))))
print(duplicated(substring(rownames(cell_attributes),0,16)))
cell_attributes <- cell_attributes[!duplicated(substring(rownames(cell_attributes),0,16)), ]
print(cell_attributes)
rownames(cell_attributes) <- substring(rownames(cell_attributes),0,16)
print(head(cell_attributes))


metadata <- seurat_integrated@meta.data
metadata <- metadata[!duplicated(substring(rownames(metadata),0,16)),]
print(head(rownames(metadata)))
rownames(metadata) <- substring(rownames(metadata),0,16)
print(head(rownames(metadata)))
print(colnames(metadata))
metadata$barcodes <- NULL
metadata$barcodes <- rownames(metadata)
print(colnames(metadata))
print(metadata$barcodes)
print(head(metadata))
cell_attributes$barcodes <- NULL
cell_attributes$barcodes <- rownames(cell_attributes)
cell_attributes <- plyr::join(cell_attributes, metadata, by="barcodes")
rownames(cell_attributes) <- cell_attributes$barcodes
cell_attributes$Row.names <- NULL
print(head(cell_attributes))


print(head(metadata))
number_samples <- length(unique(metadata$sampleID))
# optionally, read in an additional meta data table
print(empty_string)
 if (is.character(opt$add_meta)) {
#   addMeta_table <- read.csv("metadata_table.PBMC_Figure2", sep = "\t", stringsAsFactors = FALSE)
   addMeta_table <- read.csv(opt$add_meta, sep = "\t", stringsAsFactors = FALSE)
   ## Use sampleID up to (but excluding) the second "." as shorter sampleID
   addMeta_table$sampleID <- regmatches(addMeta_table$sampleID, regexpr("[^\\.]+[^\\.]+", addMeta_table$sampleID))
   print("Reading in meta data table: check if input samples and meta data sampleIDs are identical")
   print(addMeta_table$sampleID)
   stopifnot(sort(addMeta_table$sampleID) == sort(unique(metadata$sampleID)))
 } else if (is.null(opt$add_meta)) {
   print("No input file with additional meta data given.")
   addMeta_table <- NULL
 } else {
   print("Something went wrong reading in the meta data table.")
 }


# Read in lookup table

lookup <-read.csv(opt$lookup, sep = "\t", stringsAsFactors = FALSE)
lookup$Antibody <- sub("_", "-", lookup$Antibody)
print(lookup)
# Read in thresholds
thresholds <- read.csv(opt$thresholds, sep= "\t", stringsAsFactors = FALSE)
thresholds$Antibody<- sub("_", "-", thresholds$Antibody)
rownames(thresholds) <- toupper(thresholds$Antibody)
print(tail(thresholds,20))

set.seed(2)

config <- read.csv(opt$colour_config, sep = "\t", stringsAsFactors = FALSE)

config$class <- NULL
config$indication <- NULL
# have character vector of colour names from the colour config

config <- rbind(config, c("uncertain", "grey50", "uncertain"))
config <- rbind(config, c("unknown", "black", "unknown"))
# give names to colours from other column of colour config
ct.color <- config$colour
names(ct.color) <- config$mapping
print(ct.color)

# make sure "original objects" contain the correct mapping for the celltypes

# get integer vector containing the index of the matching colour in ct.color for each
# level of seurat_integrated[[]]$celltype_major
id.first.ct <- match(levels(seurat_integrated[[]]$celltype_major), names(ct.color))
id.first.ct
id.final.ct <- match(levels(seurat_integrated[[]]$celltype_final), names(ct.color))
id.final.ct

colstypes <- c("red2", "green4", "green2", "mediumorchid2", "blue2", "cyan2", "yellow1", "purple", "brown",
               "chocolate1", "chartreuse2", "darkgoldenrod3", "steelblue1", "slateblue3", "olivedrab4", "gold2",
               "violetred3", "darkcyan", "orchid3", "darksalmon", "darkslategrey", "khaki", "indianred2", "magenta", "slategray2",
               "olivedrab1", "mediumaquamarine", "hotpink", "yellow3",
               "bisque4", "darkseagreen1", "dodgerblue3",
               "deeppink4", "sienna4", "mediumorchid4")
###   Plot general cohort composition   ###
print(empty_string)
print("Plot general cohort composition.")
print(Sys.time())

#######################################
# Plot AB Expression
#######################################


# generate antibody/expression plots as it is done for the single samples
cat("\n\nCreate antibody / gene expression - plots\n")


my_merge <- function(df1, df2){
  merge(df1, df2, by = "rownames", all=TRUE)
}

DefaultAssay(seurat_integrated) <- "adt"

print(rownames(cell_attributes))

cell_attributes <- as.data.frame(Embeddings(seurat_integrated, reduction = "wnn.umap"))
cell_attributes$barcodes<- rownames(cell_attributes)
cell_attributes <- merge(cell_attributes, as.data.frame(seurat_integrated@meta.data), by=0)
rownames(cell_attributes) <- cell_attributes$barcodes
cell_attributes$Row.names <- NULL

p_final_ref <-
  ggplot(cell_attributes, aes(x = wnnUMAP_1, y = wnnUMAP_2, color = celltype_final)) +
  scale_color_manual(name = "Cell type",
                     values = ct.color[id.final.ct],
                     drop = F) +
  geom_point(size = 1) +
  theme(aspect.ratio = 1,panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black")) +
  xlab("wnnUMAP 1") +
  ylab("wnnUMAP 2") +
  theme(legend.position = "none")
p_final_ref
p_legend_ref <-
  ggplot(cell_attributes, aes(x = wnnUMAP_1, y = wnnUMAP_2, color = celltype_final)) +
  scale_color_manual(name = "Cell type",
                     values = ct.color[id.final.ct],
                     drop = F) +
  geom_point(size = 1) +
  xlab("wnnUMAP 1") +
  ylab("wnnUMAP 2") +
  guides(colour = guide_legend(override.aes = list(size = 5, shape = 15), nrow = 15)) +
  theme(
    legend.title = element_text(size = 16),
    legend.text = element_text(size = 10),
    legend.key.width = unit(0.2, "line"),
    legend.key.height = unit(0.01, "line"),
    legend.spacing.y = unit(0.1, "line"),
    legend.spacing.x = unit(0.1, "line"),
    panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
    panel.background = element_blank(), axis.line = element_line(colour = "black")
  )

p_legend_ref

legend_ref <- cowplot::get_legend(p_legend_ref)

plot_gene <- list()
# iterate over all genes from the lookup table and generate plot with gene
# and antibody expression
numberOfGenes <- length(lookup$Gene)


for (gene in 103:106){#seq(numberOfGenes)) {
  print(gene)
  merged_plots <- list()
  gene_name <- lookup$Gene[gene]
  cat("\nPlotting", as.character(gene_name), "\n")
  # Plotting both, gene and antibody name, only if they are not identical
  if (toupper(gene_name) == toupper(lookup[gene, ]$Antibody)) {
    legend <- gene_name
  } else {
    legend <- paste(gene_name, " / ", lookup[gene, ]$Antibody, sep = "")
  }
  if (!(gene_name %in% rownames(seurat_integrated@assays$SCT@counts))){
    print(paste0(gene_name," not detected."))
    expr <- ggplot(cell_attributes, aes(x = wnnUMAP_1, y = wnnUMAP_2)) +
      # all cells (obsolete?)
      geom_point(color="gray", size = 0.1)+
      ggtitle(legend) +theme_classic() +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks = element_blank(),
        plot.title = element_text(
          face = "bold",
          hjust = 0.5,
          vjust = 0.1,
          size = 16
        ),
        plot.margin = margin(1, 1, 1, 1, "pt"),
        legend.key.size = unit(0.8, "line"),
        legend.text = element_text(size = 8),
        legend.title = element_text(size = 8),
        legend.margin = margin(0.2, 0.2, 0.2, 0.2),
        axis.text = element_text(size = 5),
        aspect.ratio = 1
      )
  } else {

	     print(paste0(gene_name," detected."))
    ###   differentiate between genes with RNA-expression and without
    colour_when_zero <- NULL
    # if gene is expressed add normcounts to colData_df_gene
    if (max(seurat_integrated@assays$SCT@counts[gene_name,], na.rm = TRUE)>0) {
      colour_when_zero <- "slateblue4"
      # if gene is not expressed (not in expression matrix) add zeroes to colData_df_gene
    } else {
      colour_when_zero <- "gray"
    }
    print(colour_when_zero)
    # maximum count found for current gene
    max_count <- max(seurat_integrated@assays$SCT@counts[gene_name,])
    # upper limit of gene expression colour scale is either maximum count or 3
    upper_limit <- max(3, max_count)
  ###   plot gene expression   ###
   genes_zero_expression <- as.data.frame(seurat_integrated@assays$SCT@counts[gene_name, which(seurat_integrated@assays$SCT@counts[gene_name,]==0), ])
  my_rownames <- rownames(genes_zero_expression)
  my_new_rownames <- substring(my_rownames[!duplicated(substring(rownames(genes_zero_expression),0,16))], 0,16)
  colnames(genes_zero_expression) <- "expressed"
  genes_zero_expression <- as.data.frame(genes_zero_expression[!duplicated(substring(rownames(genes_zero_expression),0,16)), ])
  colnames(genes_zero_expression) <- "expressed"
  rownames(genes_zero_expression) <- my_new_rownames
  colnames(genes_zero_expression) <- "expressed"
  genes_zero_expression <- merge(cell_attributes, genes_zero_expression, by=0)
  genes_nonZero_expression <- as.data.frame(seurat_integrated@assays$SCT@counts[gene_name, which(seurat_integrated@assays$SCT@counts[gene_name,]> 0), ])
  my_rownames <- rownames(genes_nonZero_expression)
  my_new_rownames <- substring(my_rownames[!duplicated(substring(rownames(genes_nonZero_expression),0,16))], 0,16)
  genes_nonZero_expression <- as.data.frame(genes_nonZero_expression[!duplicated(substring(rownames(genes_nonZero_expression),0,16)), ])
rownames(genes_nonZero_expression) <- my_new_rownames
  colnames(genes_nonZero_expression) <- "expressed"
  genes_nonZero_expression <- merge(cell_attributes, genes_nonZero_expression, by=0)
   DefaultAssay(seurat_integrated) <- "SCT"
  expr <- FeaturePlot(seurat_integrated, features = gene_name, slot="counts",
                     reduction = 'wnn.umap', max.cutoff = "q99",  ncol = 1)
  expr
  expr <- expr +
    geom_point(data = genes_zero_expression,
               aes(x = wnnUMAP_1,
                   y = wnnUMAP_2),
               colour = "slateblue4", size = 0.1) +
    # cells with non-zero expression
    geom_point(data = genes_nonZero_expression,
               aes(x = wnnUMAP_1,
                   y = wnnUMAP_2,
                   color = expressed,
               ),size = 0.1)+
    scale_color_gradientn(
      name = "counts",
      na.value = "gray",
      colours = c("slateblue3", "royalblue1", "aquamarine3", "khaki", 383, "sienna1", "orangered4"),
      limits = c(1, max(3, upper_limit)),
      breaks = c(floor(upper_limit / 3), round(2 * (upper_limit / 3)), upper_limit)
    ) +
    ggtitle(legend) +
    theme(
      axis.title.x = element_blank(),
      axis.title.y = element_blank(),
      axis.text.y = element_blank(),
      axis.ticks = element_blank(),
      axis.text.x = element_blank(),
      #   face = "bold", hjust = 0.5, vjust = 0.1, size = 14),
      plot.margin = margin(1, 1, 1, 1, "pt"),
      legend.key.size = unit(0.8, "line"),
      legend.text = element_text(size = 8),
      legend.title = element_text(size = 8),
      legend.margin = margin(0.2, 0.2, 0.2, 0.2),
      axis.text = element_text(size = 5),
      aspect.ratio = 1,
      plot.title = element_text(
        face = "bold",
        hjust = 0.5,
        vjust = 0.1,
        size = 16
      ),
    )
  expr
  }
  # combine colData_df with antibody
  my_ab <- lookup[gene, ]$Antibody
  my_ab_df <- t(as.data.frame(seurat_integrated@assays$adt@counts))
  colnames(my_ab_df) <- toupper(colnames(my_ab_df))
  rownames(cell_attributes) <- cell_attributes$barcodes.x
  colData_df_antibody <- merge(cell_attributes, my_ab_df, by=0)
  rownames(colData_df_antibody) <- colData_df_antibody$Row.names
  if (paste0("ADT-",toupper(my_ab)) %in% colnames(my_ab_df)){
    colData_df_antibody$expressed <- log(colData_df_antibody[,paste0("ADT-",toupper(my_ab))]+1)
  }  else {
    colData_df_antibody$expressed <- 0}
  print(head(my_ab_df[,paste0("ADT-",toupper(my_ab))]))
  print(head(my_ab_df[,paste0("ADT-",toupper(my_ab))]+1))
  print(head(colData_df_antibody$expressed))
  #
  colData_df_antibody$expressed_thresholds <- colData_df_antibody$expressed
  min_threshold <- 0
  for (sample in unique(colData_df_antibody$sampleID)){
    my_threshold <- thresholds[toupper(my_ab),sample]
    print(my_threshold)
    if (my_threshold >0 & my_threshold < min_threshold){
      min_threshold <- my_threshold
      print("min_threshold")
      print(min_threshold)
    }
    set_to_na <- which(colData_df_antibody[colData_df_antibody$sampleID==sample,]$expressed<my_threshold)
    if (length(set_to_na) >0){
    colData_df_antibody[colData_df_antibody$sampleID==sample,]$expressed_thresholds[set_to_na] <- NA
    }
  }
  colData_df_antibody$expressed <- colData_df_antibody$expressed_thresholds
  print(head(colData_df_antibody$expressed_thresholds))
  #if (colData_df_antibody$expressed < thresholds[])
# take natural (ln) log of ADT counts
  upper_limit <- round(max(min_threshold * 3, max(colData_df_antibody$expressed, na.rm=TRUE), 3, na.rm = TRUE),1)
  medium_break <- round((min_threshold + upper_limit) / 2,1)
  lower_break <- round((medium_break + min_threshold) / 2,1)

  ###   differentiate between antibodies that are expressed and those that are not
  # if antibody is not expressed
  if (all(colData_df_antibody$expressed == 0)) {
    print(paste0(my_ab," ab not detected."))
    print(head(colData_df_antibody))
    antibody_plot <- ggplot(as.data.frame(colData_df_antibody),
                            aes(x = wnnUMAP_1, y = wnnUMAP_2, color = expressed)) +
      geom_point(data = colData_df_antibody$expressed,
                 size = 0.1,
                 colour = "gray") +
     coord_fixed(ratio = 1) +
      theme_classic() +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks = element_blank(),
        plot.title = element_text(
          face = "bold",
          hjust = 0.5,
          vjust = 0.1,
          size = 8
        ),
        plot.margin = margin(0.5, 0.5, 0.5, 0.5, "pt"),
        legend.key.size = unit(0.8, "line"),
        legend.text = element_text(size = 8),
        legend.title = element_text(size = 8),
        legend.margin = margin(0.2, 0.2, 0.2, 0.2),
        axis.text = element_text(size = 5),
        aspect.ratio = 1
      ) +
      guides(fill = guide_legend(title = "counts"))
    # if antibody is expressed
  } else {
    DefaultAssay(seurat_integrated) <- "adt"
    print(rownames(seurat_integrated@assays$adt@counts))
    antibody <- FeaturePlot(seurat_integrated, features = paste0("ADT-",my_ab), slot="counts",
                            reduction = 'wnn.umap', max.cutoff = "q99",  ncol = 1)
    print(antibody)
    antibody <- antibody +
      geom_point(data = colData_df_antibody[is.na(colData_df_antibody$expressed), ],
                 aes(x = wnnUMAP_1,
                     y = wnnUMAP_2),
                 colour = "gray", size = 0.1) +
      # cells with non-zero expression
      geom_point(data = colData_df_antibody[!is.na(colData_df_antibody$expressed), ],
                 aes(x = wnnUMAP_1,
                     y = wnnUMAP_2,
                     color = expressed,
                 ),size = 0.1)+
      scale_color_gradientn(
        name = "counts",
        na.value = "gray",
        colours = c("slateblue3", "royalblue1", "aquamarine3", "khaki", 383, "sienna1", "orangered4"),
        limits = c(1, max(3, upper_limit)),
        breaks = c(floor(upper_limit / 3), round(2 * (upper_limit / 3)), upper_limit)
      )
    print(antibody)
    print(paste0(my_ab," detected."))
    print(head(colData_df_antibody$expressed))
    antibody_plot <- ggplot(colData_df_antibody,
                            aes(x = wnnUMAP_1, y = wnnUMAP_2, color = expressed)) +
      geom_point(
                 size = 0.1) +
      scale_color_gradientn(
        name = "ln counts",
        na.value = "gray",
        colours = c("slateblue4", "royalblue1", "aquamarine3", "khaki", 383, "sienna1", "orangered4") ,
        limits = c(min_threshold, upper_limit),
        breaks = c(min_threshold,  medium_break, upper_limit),
        labels = c(paste0("<", min_threshold),
                   medium_break,
                   upper_limit)
      ) +
      coord_fixed(ratio = 1) + theme_classic() +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks = element_blank(),
        plot.title = element_text(
          face = "bold", hjust = 0.5, vjust = 0.1, size = 8),
        plot.margin = margin(0.5, 0.5, 0.5, 0.5, "pt"),
        legend.key.size = unit(0.8, "line"),
        legend.text = element_text(size = 8),
        legend.title = element_text(size = 8),
        legend.margin = margin(0.2, 0.2, 0.2, 0.2),
        axis.text = element_text(size = 5),
        aspect.ratio = 1
      ) +
      guides(fill = guide_legend(title = "counts"))
    print(antibody_plot)
  }
  antibody_plot
  # combine expression plot and antibody plot of current gene to one plot
  combined_plot  <- expr / antibody_plot
  plot_gene[[gene]] <- combined_plot
  ggplot2::ggsave(
    filename = paste("Test_RNAdataSlot", gene_name, "plot.png", sep = "_"),
    width = 21,
    height = 14,
    plot = plot_gene[[gene_name]])
  print("end")
}
layout <- c(
  area(1, 2, 5, 6),
  area(1, 7, 5, 11),
  area(1, 12, 5, 17),
  area(6, 1, 8, 4),
  area(6, 4, 8, 7),
  area(6, 7, 8, 10),
  area(6, 10, 8, 13),
  area(6, 13, 8, 16),
  area(6, 16, 8, 19),
  area(9, 1, 11, 4),
  area(9, 4, 11, 7),
  area(9, 7, 11, 10),
  area(9, 10, 11, 13),
  area(9, 13, 11, 16),
  area(9, 16, 11, 19)
)

###   Save expression plots   ###
cat("\nSaving Expression Plots:\n")
plot_gene2=list()
 plot_gene2[[1]]=plot_gene[[103]]
 plot_gene2[[2]]=plot_gene[[104]]
 plot_gene2[[3]]=plot_gene[[105]]
 plot_gene2[[4]]=plot_gene[[106]]
plot_gene=plot_gene2
numberOfPlots <- length(plot_gene)
numberOfPlots
# only plot 12 plots into one file
numberOfPages <- numberOfPlots %/% 12
if (numberOfPlots %% 12 != 0) {
  numberOfPages <-  numberOfPages + 1
}

for(i in 1:4){
ggsave(filename=paste0(i,".png"),plot=plot_gene[[i]],width=21,height=14)
}
