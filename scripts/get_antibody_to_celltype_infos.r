suppressPackageStartupMessages({
library(optparse)
library(scran)
library(dplyr)
library(Seurat)
library(reshape)
})


# parse command line arguments
option_list = list(
make_option("--sce_in", type = "character", help = "Path to RDS input file that contains sce object."),
make_option("--cellranger", type = "character", help = "Path to Cellranger output folder that contains feature barcoding analysis."),
make_option("--sample_name", type = "character", help = "Sample name that will be the prefix of all output files."),
make_option("--outDir", type = "character", help = "Full path to output directory")
)
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

# read in feature barcoding analysis
umi_counts=Read10X(opt$cellranger)

GEX=as.matrix(umi_counts$'Gene Expression')
ADT=as.matrix(umi_counts$'Antibody Capture')

# read in sce object from RDS file & select important columns
my_sce <- readRDS(opt$sce_in)

df_colData_cells <- as.data.frame(colData(my_sce), stringsAsFactors = FALSE)
pipeline_file<-select(df_colData_cells,barcodes, phenograph_clusters,celltype_final)

# intersect Pipeline file & ADT infos
# transform ADT matrix, should we go for antibody with ma 

antibodies<-rownames(ADT)
barcodes <- colnames(ADT)
ADT=t(ADT)
print(colnames(ADT))
antibody_list <- apply(ADT, 1, function(x) paste(antibodies[which(x!=0,arr.ind=T)],collapse=','))
print(dim(antibody_list))
df <- melt(data.frame(barcodes,antibody_list))
colnames(df) <- c("barcodes","antibodies")
