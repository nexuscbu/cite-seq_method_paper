# Script is work in progress.
# To do:
# Take imput as arguments. 
# Adapt Vignette Naming of Variables to more general names. 
# Can we join the hashing matrices from different lanes?



library("Seurat")


args = commandArgs(trailingOnly=TRUE)

umi_counts_folder=args[1]
hashtag_counts_folder=args[2]
sample_name=args[3]
umi_counts=Read10X(umi_counts_folder)


# Assuming Cell Ranger count was run on a ADT Sample, the umi_counts object now is a list containing "Gene Expression" Data as one entry and "Antibody Capture" as the second. 
GEX=as.matrix(umi_counts$'Gene Expression')
ADT=as.matrix(umi_counts$'Antibody Capture')

# Print some general statistics, just to see if we understand the format. (Numbers can be compared to the Cell-Ranger report)$

print("Genes detected:")
sum(rowSums(as.matrix(GEX) != 0)!=0)
print("Estimated cells:")
dim(GEX)[2]

# Is this the correct way to do that? (Maybe 0 is a to lenient filter?)


rowSums(ADT>5)
write.csv(rowSums(ADT >5),file=paste("Antibody_Barcodes_Stat", sample_name,"txt",sep="."))




# Read in Hashtag_counts.
hashtag_counts=Read10X(hashtag_counts_folder, gene.column=1)

# Select cell barcodes detected by both RNA and HTO In the example datasets we have already
# filtered the cells for you, but perform this step for clarity.
joint.bcs <- intersect(colnames(GEX), colnames(hashtag_counts))
#print(joint.bcs)

# Subset RNA and HTO counts by joint cell barcodes
GEX <- GEX[, joint.bcs]
hashtag_counts <- as.matrix(hashtag_counts[, joint.bcs])

rownames(hashtag_counts)
dim(hashtag_counts)


# Setup Seurat object
pbmc.hashtag <- CreateSeuratObject(counts = GEX)
# Normalize RNA data with log normalization
pbmc.hashtag <- NormalizeData(pbmc.hashtag)
print(pbmc.hashtag)


# Find and scale variable features
pbmc.hashtag <- FindVariableFeatures(pbmc.hashtag, selection.method = "mean.var.plot")
pbmc.hashtag <- ScaleData(pbmc.hashtag, features = VariableFeatures(pbmc.hashtag))


# Add HTO data as a new assay independent from RNA
pbmc.hashtag[["HTO"]] <- CreateAssayObject(counts = hashtag_counts)
# Normalize HTO data, here we use centered log-ratio (CLR) transformation

pbmc.hashtag <- NormalizeData(pbmc.hashtag, assay = "HTO", normalization.method = "CLR")
print("REACHED HERE")
pbmc.hashtag <- HTODemux(pbmc.hashtag, assay = "HTO", verbose = TRUE)

write.table(pbmc.hashtag$HTO_classification.global, file=paste(sample_name,"_Stats.txt",sep=""))

Idents(pbmc.hashtag) <- "HTO_maxID"
pdf(paste(sample_name,"Ridgeplot.pdf", sep="_"),width=30, height=7)
mar.default <- c(5,4,4,2) + 0.8
par(mar = mar.default + c(0, 8, 0, 0)) 
RidgePlot(pbmc.hashtag, assay = "HTO", features = rownames(pbmc.hashtag[["HTO"]])[1:4], ncol = 4)

## Creating DimPlots using Hashtags.
# Calculate a distance matrix using HTO
hto.dist.mtx <- as.matrix(dist(t(GetAssayData(object = pbmc.hashtag, assay = "HTO"))))

# Calculate tSNE embeddings with a distance matrix
pbmc.hashtag <- RunTSNE(pbmc.hashtag, distance.matrix = hto.dist.mtx, perplexity = 100)
pdf(paste(sample_name,"DimPlot_Hashtag.pdf",sep="_"),width=12, height=7)
mar.default <- c(5,4,4,2) + 0.8
par(mar = mar.default + c(0, 8, 0, 0))
DimPlot(pbmc.hashtag)

## Creating DimPlots using Celltype Classification.
Idents(pbmc.hashtag) <- "HTO_classification.global"
# Calculate a distance matrix using HTO
hto.dist.mtx <- as.matrix(dist(t(GetAssayData(object = pbmc.hashtag, assay = "HTO"))))
# Calculate tSNE embeddings with a distance matrix
pbmc.hashtag <- RunTSNE(pbmc.hashtag, distance.matrix = hto.dist.mtx, perplexity = 100)
pdf(paste(sample_name,"DimPlot_Classification.pdf",sep="_"),width=12, height=7)
mar.default <- c(5,4,4,2) + 0.8
par(mar = mar.default + c(0, 8, 0, 0))
DimPlot(pbmc.hashtag)


# Extract the singlets
pbmc.singlet <- subset(pbmc.hashtag, idents = "Singlet")

# Select the top 1000 most variable features
pbmc.singlet <- FindVariableFeatures(pbmc.singlet, selection.method = "mean.var.plot")

# Scaling RNA data, we only scale the variable featurepbmc.hashtag <- RunTSNE(pbmc.hashtag, distance.matrix = hto.dist.mtx, perplexity = 100)

pbmc.singlet <- ScaleData(pbmc.singlet, features = VariableFeatures(pbmc.singlet))

# Run PCA
pbmc.singlet <- RunPCA(pbmc.singlet, features = VariableFeatures(pbmc.singlet))

# We select the top 10 PCs for clustering and tSNE based on PCElbowPlot
pbmc.singlet <- FindNeighbors(pbmc.singlet, reduction = "pca", dims = 1:10)
pbmc.singlet <- FindClusters(pbmc.singlet, resolution = 0.6)


pbmc.singlet <- RunTSNE(pbmc.singlet, reduction = "pca", dims = 1:10)


pdf(paste(sample_name,"DimPlot_Singlets.pdf",sep="_"),width=12, height=7)
mar.default <- c(5,4,4,2) + 0.8
par(mar = mar.default + c(0, 8, 0, 0))
# Projecting singlet identities on TSNE visualization
DimPlot(pbmc.singlet, group.by = "HTO_classification")

