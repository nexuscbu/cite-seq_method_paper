### SCRIPT TO ANALYSE HASHING 


# Change the parameters in the following section to adapt for a specific sample.
##########################
# Input for CiteSeq Count:
# Inputprefix needs to be from ADT fastqs until the lane encription.
INPUTPREFIX="/cluster/project/nexus/phrt/levesque_melanoma_2019/data/20191130/PBMC_untreated/fastqs_ADT/BSSE_QGF_132645_HHVFNDRXX_1_PBMC_untreated_5k_ADT_E1_SI-NA-E1_S3_"
tags_file="/cluster/project/nexus/linda/git_eth/levesque_melanoma_2019/metadata/tags.20191130.PBMC_untreated.csv"


# Input for CellRanger ADT Only Analysis
# The following files need to be prepared according to this:
# https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/feature-bc-analysis
feature_reference_file="/cluster/project/nexus/linda/git_eth/levesque_melanoma_2019/metadata/feature_reference.20191130.txt"
libraries_file="/cluster/project/nexus/linda/git_eth/levesque_melanoma_2019/metadata/libraries.PBMCuntreated.txt"

#Experiment specific parameters
NUMBEROFLANES=2
TARGETEDCELLS=5000

#Dataanalysis specific parameters:
OUTDIR="/cluster/project/nexus/phrt/levesque_melanoma_2019/data/20191130/PBMC_untreated/analysis_pooledSample/"
SAMPLEID="PBMC_untreated"
REFERENCE="/cluster/project/nexus/utilities/databases/singlecell/10xGenomics/gene_expression_3_0_2/refdata-cellranger-GRCh38-3.0.0/"
########################



# Analysis part starts here. 
# Define individual output folders:
if not "CITESEQOUT" in globals():
	CITESEQOUT=OUTDIR+"citeseq/"
if not "ANALYSISOUT" in globals():
	ANALYSISOUT=OUTDIR + "hashingAnalysis/"
if not "CELLRANGEROUT" in globals():
	CELLRANGEROUT=OUTDIR + "CellRangerADT_afterIndexHoppingRemoval/"

# Define lane function:
def getLaneNames():
	output= []
	for lane in range(NUMBEROFLANES):
		output.append("L00"+str(lane+1))
	print(output)
	return output

localrules: all

rule all:
	input:
		expand(CITESEQOUT + '{lanename}/umi_count/barcodes.tsv.gz', lanename=getLaneNames()),
		cellranger = CELLRANGEROUT + SAMPLEID + "/outs/filtered_feature_bc_matrix/barcodes.tsv.gz",
		classification = ANALYSISOUT +"hashtag.classification.png"


# Analyse hashing results:

rule analyseCiteseqResults:
	input:
		lanes = expand(CITESEQOUT + "{lane}/umi_count/", lane=getLaneNames()),
		adt = CELLRANGEROUT + SAMPLEID + "/outs/filtered_feature_bc_matrix/barcodes.tsv.gz"
	output:
		rawCounts = ANALYSISOUT + "raw_tag_counts.txt",
		finalCounts = ANALYSISOUT +"final_tag_counts.txt",
		ridgeplot = ANALYSISOUT + "ridgeplot.png",
		classification = ANALYSISOUT +"hashtag.classification.png",
	params:
		prefix = ANALYSISOUT + SAMPLEID,
		adt_folder = CELLRANGEROUT + SAMPLEID + "/outs/filtered_feature_bc_matrix/",
		lsfoutfile=ANALYSISOUT + "AnalysisOut.lsfout.txt",
		lsferrfile=ANALYSISOUT + "AnalysisOut.lsferr.txt",
		mem = 5000,
		time = 360,
		scratch= 2000
	threads: 1
	shell:
		"lanes=`echo {input.lanes} | sed 's/ /,/g'` ; " 
		"Rscript analyseHashing.R --citeseq_in $lanes --adt_barcodes_in {params.adt_folder} --output_prefix {params.prefix}"


# Run CellRanger on an ADT sample. Hashing  analysis is then performed only on barcodes detected by CiteSeq and CellRanger. 
# (Currently the recommended approach by Seurat (Aug. 2019)

rule runCellRangerCount:
	input:
		library=libraries_file,
		features=feature_reference_file
	output:
		outfile=CELLRANGEROUT + SAMPLEID + "/outs/web_summary.html",
		barcodes=CELLRANGEROUT + SAMPLEID + "/outs/filtered_feature_bc_matrix/barcodes.tsv.gz"
	params:
		sampleID=SAMPLEID,
		reference=REFERENCE,
		lanes=NUMBEROFLANES,
		analysisdir=CELLRANGEROUT,
		cells=TARGETEDCELLS,
		lsfoutfile=CELLRANGEROUT + "CellRangerCount.lsfout.txt",
                lsferrfile=CELLRANGEROUT + "CellRangerCount.lsferr.txt",
                mem = 10000,
                time = 360,
                scratch= 2000
	threads: 8
	shell:
		'(cd {params.analysisdir}; /cluster/project/nexus/utilities/sharedPrograms/cellranger/cellranger-3.1.0/cellranger count --id={params.sampleID} --force-cells {params.cells} --transcriptome={params.reference} --localcores={threads} --feature-ref={input.features} --libraries={input.library} --nosecondary )'


# Run Cite Seq on all available lanes individually.
# ToDo: Keep an eye open on  possible changes in the script. Eventually it might be possible to run it on all lanes at once. 
rule citeseqCount:
	input:
		R1=INPUTPREFIX + "{lane}_R1_001.fastq.gz",
		R2=INPUTPREFIX + "{lane}_R2_001.fastq.gz",
		tags=tags_file,
		barcodes=CELLRANGEROUT + SAMPLEID + "/outs/filtered_feature_bc_matrix/barcodes.tsv.gz"
	output:
		out=CITESEQOUT + "{lane}/umi_count/barcodes.tsv.gz",
		lanes = directory(CITESEQOUT + "{lane}/umi_count/")
	params:
		lsfoutfile=CITESEQOUT + "citeseqCount.{lane}.lsfout.txt",
		lsferrfile=CITESEQOUT + "citeseqCount.{lane}.lsferr.txt",
		whitelist=CITESEQOUT + "whitelist.txt",
		mem = 5000,
		time = 360,
		scratch= 2000,
		cells=TARGETEDCELLS,
		outfolder=CITESEQOUT + "{lane}/"
	threads: 8
	shell:
		"zcat {input.barcodes} > {params.whitelist} ; "
		"sed -i -e s/-1//g {params.whitelist} ;"
		" module load new ;module load gcc/6.3.0 ;  module load python/3.7.1 ; /cluster/home/lgrob/.local/bin/CITE-seq-Count -t {input.tags} -R1 {input.R1} -R2 {input.R2} -T {threads} -wl {params.whitelist} -cbf 1 -cbl 16 -umif 17 -umil 26 -cells {params.cells} -o {params.outfolder} --start-trim 10" 


