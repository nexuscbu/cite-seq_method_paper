
library(optparse)
library(Hmisc)

option_list = list(
make_option("--samples", type = "character", help = "list of samples, separated by comma"),
make_option("--outname", type = "character"),
make_option("--lookup", type = "character", default = "reference_lookup.tab"))
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

lookup <- read.delim(opt$lookup)
lookup[which(lookup[,2]=="CD4_RPA-T4"),2] <- "CD4-RPA-T4"
lookup[which(lookup[,2]=="CD8a_RPA-T8"),2] <- "CD8a-RPA-T8"
lookup[which(lookup[,2]=="TCR-a_b"),2] <- "TCR-a-b"

count_table <- NULL
is_first = T
for (mysample in samples){
message(mysample)

message("reading file")
tmp <- readRDS(paste0("aggregation_rna/samplesRDS/", mysample, "__sample_name.RDS"))
tmp2 <- readRDS(paste0("aggregation_adt/samplesRDS/", mysample, "__sample_name.RDS"))

tmp_count_table <- rbind(as.data.frame(tmp), as.data.frame(tmp2))
if(is_first){
count_table <- tmp_count_table
is_first=F
}else{
count_table <- cbind(count_table, tmp_count_table[rownames(count_table), ])
}
}
colnames(count_table) <- samples
count_table <- t(count_table)

#accelerate the correlation by keeping only the entries of interest
lookup_tmp <- (lookup)
lookup_tmp[,2] <- paste0("ADT-", lookup[,2])
lookup_tmp <- unlist(lookup_tmp)
cols_to_keep <- which(colnames(count_table) %in% lookup_tmp)
count_table <- count_table[,cols_to_keep]

message("correlation")
correlation_table <- rcorr(count_table, type="pearson")

correlation_df <- NULL
pval_df <- NULL
tmp_corr_df <- NULL
tmp_pval_df <- NULL

message("looping")
for(i in 1:nrow(correlation_table$r)){
  current_gene <- rownames(correlation_table$r)[i]
  if(length(grep("^ADT-", current_gene))>0){
    next()
  }
  lookup_row <- which(lookup[,1] == current_gene)
  if(length(lookup_row) == 0){next()}
  for(j in 1:length(lookup_row)){
  message(j)
  lookup_adt <- paste0("ADT-",lookup[lookup_row[j],2])
  current_adt_col <- which(colnames(correlation_table$r) == lookup_adt)

  tmp_corr_df <- rbind(tmp_corr_df, data.frame(tmpname= correlation_table$r[i, current_adt_col]) )
  rownames(tmp_corr_df)[nrow(tmp_corr_df)] <- paste0(current_gene, "_", lookup_adt)
  tmp_pval_df <- rbind(tmp_pval_df, data.frame(tmpname= correlation_table$P[i, current_adt_col]) )
  rownames(tmp_pval_df)[nrow(tmp_pval_df)] <- paste0(current_gene, "_", lookup_adt)
}}
correlation_df <- tmp_corr_df
pval_df <- tmp_pval_df

saveRDS(correlation_df, "correlation_df.RDS")
saveRDS(pval_df, "pval_df.RDS")

 tmp=cbind(correlation_df, pval_df)
colnames(tmp) <- c("r", "P")
tmp=tmp[which(!is.nan(tmp[,1])),]

p=ggplot(data=tmp, aes(x=reorder(rownames(tmp), r), y=r)) + geom_bar(stat="identity", aes(fill=-log10(P))) + theme(axis.text.x =element_text(angle=90,hjust=1,vjust=0.5)) + scale_fill_gradient2(midpoint=-log10(0.05), low="grey", mid="blue", high="red") + xlab("") + ylab("Correlation coefficient r") + labs(fill="-log10(PValue)") + ggtitle(paste0(opt$outname, " - selected Gene-protein expression correlation")) #+ geom_vline(xintercept=0.5, linetype="dotted", color = "red", size = 1.5) + geom_vline(xintercept=40.5, linetype="dotted", color = "red", size = 1.5)
ggsave(plot=p, filename=paste0(opt$outname,"_correlation_plot_005_final.pdf"), width=20 )

