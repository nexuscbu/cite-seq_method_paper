########################################

####    File name: filter_genes_and_cells.R
####    Author: Anne Richter
####    Created in October 2018
####    R Version: 3.5.1

########################################

# Filtering of non-coding genes was included into this script
# Description:
## File name: select_protein_coding_genes.R
## Author: Dr. Franziska Singer and Mustafa Anil Tuncel
## Date created: 22.03.2018
## R Version: 3.4.1

#######################################


# This script takes an hdf5 file of raw expression counts (right after cellranger)

# Filtering Steps:
# 1. Cells are filtered if number of detected genes (NODG) is too low.
# 2. Cells are filtered if the fraction of reads mapped to MT- genes is too high.
# 3. Genes are filtered so that only protein-coding genes are kept.
# 4. Genes are filtered so that MT- genes are removed
# 5. Genes are filtered so that genes encoding for ribosomal proteins are removed

# Filtering is based on the scRNA tutorial by Atul Sethi and Panagiotis Papasaikas on ECCB in Athens

# R libraries
library(rhdf5)
library(optparse)
library(plyr)
library(ggplot2)
library(RColorBrewer)
library(biomaRt)
suppressMessages(library(scater))

# convenience function for string concatenation
'%&%' = function(a,b) paste(a,b,sep="")
# convenience function for NOT %in%
'%!in%' <- function(x,y)!('%in%'(x,y))

# parse command line arguments
option_list = list(
make_option("--hdf5File", type = "character", help = "Path to hdf5 input file. It includes raw expression matrix, gene & cell attributes."),
make_option("--nmads_NODG", type = "character", help = "Number of median-absolute-deviations away from median required for a value to be called an outlier, e.g. 5"),
make_option("--nmads_fractionMT", type = "character", help = "Number of median-absolute-deviations away from median required for a value to be called an outlier, e.g. 5"),
make_option("--outDir", type = "character", help = "Full path to output directory"),
make_option("--genomeVersion", type = "character", help = "Specify the genome annotation version, either hg19 or GRCh38 are supported."),
make_option("--threshold_NODG", type = "character", help = "Hard threshold that gives the minimum NODG a cell must have to be further processed. E.g. 250"),
make_option("--threshold_fractionMT", type = "character", help = "Hard threshold that gives the maximum fraction of MT reads a cell can have and be further processed. E.g. 0.5"),
make_option("--barcodes_keep", type = "character", help = "Path to file containing cell barcodes that are singlets (and not doublets)")
)
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)


#####     READ IN DATA     #####

# get file name of input hdf5 and sample name
file_name = basename(opt$hdf5File)
print(file_name)
# get file extension of input file
file_extens <- tail(unlist(strsplit(file_name, split = "[.]")), n=1)
print("file_extens:")
print(file_extens)
file_name_clipped = gsub(pattern = file_extens, "", file_name)
print("file_name_clipped:")
print(file_name_clipped)
outdir = opt$outDir
print("outdir:")
print(outdir)

# print information about input hdf5 file
print(h5ls(opt$hdf5File))

# get count matrix
umi_counts <- h5read(opt$hdf5File, "raw_counts")
# link cell and gene information to matrix
rownames(umi_counts) <- h5read(opt$hdf5File, "gene_attrs/gene_ids")
colnames(umi_counts) <- h5read(opt$hdf5File, "cell_attrs/cell_names")
print("dim(umi_counts):")
print(dim(umi_counts))

# get vector with cell_names
input_cell_names <- h5read(opt$hdf5File, "cell_attrs/cell_names")
print("length(input_cell_names):")
print(length(input_cell_names))

# create data frame with mapping of gene_ids to hgnc gene_names
ENS2HGNC <- as.data.frame(h5read(opt$hdf5File, "gene_attrs/gene_ids"))
names(ENS2HGNC) <- "ensembl_gene_id"
ENS2HGNC$hgnc_symbol <- h5read(opt$hdf5File, "gene_attrs/gene_names")
rownames(ENS2HGNC) <- h5read(opt$hdf5File, "gene_attrs/gene_ids")
print("head(ENS2HGNC):")
print(head(ENS2HGNC))

print("length(ENS2HGNC$ensembl_gene_id)")
length(ENS2HGNC$ensembl_gene_id)



print("#####                                  #####")
print("#####          CELL FILTERING          #####")
print("#####                                  #####")

### Filtering according to fraction of MT- reads
print("####################                  Filtering cells according to fraction of reads mapped to MT-genes                 #######################")
nmads_fractionMT <- as.integer(opt$nmads_fractionMT)

# Grep all mitochondrial genes:
mt <- as.vector(ENS2HGNC[rownames(umi_counts),1][grep("^MT-", ENS2HGNC[rownames(umi_counts),2])])
print("Number of MT- genes:")
print(length(mt))
print("Gene IDs of all MT- genes:")
print(mt)

# Calculate fraction of MT reads per cell:
fractionMTreads=colSums(umi_counts[mt,])/colSums(umi_counts)
print("head(fractionMTreads):")
print(head(fractionMTreads))
# find outliers in terms of MT-fraction
MToutliers <- isOutlier(metric = fractionMTreads, log = FALSE, nmads = nmads_fractionMT, type = "higher")
print("head(MToutliers):")
print(head(MToutliers))
print("sum(MToutliers):")
print(sum(MToutliers))
high_MT = which(MToutliers)
perc_high_MT <- signif((length(high_MT)/length(input_cell_names))*100, digits = 2)
# print out numbers of filtered cells:
print("Total number of cells:")
print(length(input_cell_names))
print("Cells filtered because fraction of reads mapped to MT- genes makes them outliers:")
print(length(high_MT))
print("Percentage of cells filtered because fraction of reads mapped to MT- genes makes them outliers:")
print(paste(perc_high_MT, "%"))

# Filter cells according to hard threshold of fraction of MT- reads
threshold_MT <- as.numeric(opt$threshold_fractionMT)
print("threshold_MT:")
print(threshold_MT)
higher_than_threshold <- fractionMTreads > threshold_MT
print("head(higher_than_threshold):")
print(head(higher_than_threshold))
high_abs_MT <- which(higher_than_threshold)
print("head(high_abs_MT):")
print(head(high_abs_MT))
perc_high_threshold_MT <- signif((length(high_abs_MT)/length(input_cell_names))*100, digits = 2)
# print out numbers of filtered cells:
print("Total number of cells:")
print(length(input_cell_names))
print("Cells filtered because fraction of reads mapped to MT- genes is higher than threshold:")
print(length(high_abs_MT))
print("Percentage of cells filtered because fraction of reads mapped to MT- genes is higher than threshold:")
print(paste(perc_high_threshold_MT, "%"))


### Filtering according to Number of detected genes (NODG)
print("####################                  Filtering cells according to number of detected genes                 #######################")
nmads_NODG <- as.integer(opt$nmads_NODG)
# Number of detected genes:
NODG=colSums(umi_counts>0)
# identify cells with NODG lower than threshold
outlierNODG <- isOutlier(metric = NODG, log = FALSE, type = "lower", nmads = nmads_NODG)
print("sum(outlierNODG):")
print(sum(outlierNODG))
print("head(outlierNODG):")
print(head(outlierNODG))
low_NODG=which(outlierNODG)

# print out numbers of filtered cells
perc_low_NODG <- signif((length(low_NODG)/length(input_cell_names))*100, digits = 2)
print("Total number of cells:")
print(length(input_cell_names))
print("Cells filtered because NODG makes them outliers:")
print(length(low_NODG))
print("Percentage of cells filtered because NODG makes them outliers:")
print(paste(perc_low_NODG, "%"))

# Filter cells according to hard threshold of NODG
threshold_NODG <- as.numeric(opt$threshold_NODG)
print("threshold_NODG:")
print(threshold_NODG)
lower_than_threshold <- NODG < threshold_NODG
print("head(lower_than_threshold):")
print(head(lower_than_threshold))
low_abs_NODG <- which(lower_than_threshold)
print("head(low_abs_NODG):")
print(head(low_abs_NODG))
# print out numbers of filtered cells
perc_low_threshold_NODG <- signif((length(low_abs_NODG)/length(input_cell_names))*100, digits = 2)
print("Total number of cells:")
print(length(input_cell_names))
print("Cells filtered because NODG is lower than threshold:")
print(length(low_abs_NODG))
print("Percentage of cells filtered because NODG is lower than threshold:")
print(paste(perc_low_threshold_NODG, "%"))


# Filter out doublets
print("Keep only singlets:")
barcodes_keep <- read.csv(opt$barcodes_keep, header = FALSE, stringsAsFactors = FALSE)
is_doublet <- !(colnames(umi_counts) %in% barcodes_keep$V1)
names(is_doublet) <- colnames(umi_counts)
doublets <- which(is_doublet)
print("head(doublets):")
print(head(doublets))
# print out numbers of filtered cells
perc_doublets <- signif((length(doublets)/length(input_cell_names))*100, digits = 2)
print("Total number of cells:")
print(length(input_cell_names))
print("Cells filtered because they are doublets: ")
print(length(doublets))
print("Percentage of cells filtered because they are doublets:")
print(paste(perc_doublets, "%"))



### Apply filters combined
# Merge all filtered cells:
filtered_cells=unique(c(low_NODG, high_MT, high_abs_MT, low_abs_NODG, doublets))
# get cell barcodes of cell that will be filtered out
names_filtered_cells <- unique(names(c(low_NODG, high_MT, high_abs_MT, low_abs_NODG, doublets)))
print("str(names_filtered_cells):")
print(str(names_filtered_cells))
print("head(names_filtered_cells):")
print(head(names_filtered_cells))

print("Total number of cells filtered from dataset:")
print(length(unique(c(low_NODG,high_MT, high_abs_MT, low_abs_NODG, doublets))))
both_filters <- intersect(low_NODG,high_MT)
perc_filtered_out <- signif((length(unique(c(low_NODG,high_MT, high_abs_MT, low_abs_NODG, doublets)))/length(input_cell_names))*100, digits = 2)

# Write file with the cell barcodes of the cells that are filtered out
txtname = outdir %&% file_name %&% "." %&% nmads_fractionMT %&% "_nmads_fractionMT." %&% nmads_NODG %&% "_nmads_NODG.filtered_cells.txt"
write.table(names_filtered_cells, txtname, sep = "\t", row.names = F, col.names = F, quote = F)

# Remove filtered cells from the dataset:
clean_umi_counts = umi_counts[,!colnames(umi_counts) %in% names_filtered_cells]
print("Total number of cells before filtering:")
print(length(input_cell_names))
print("Number of cells after filtering:")
print(length(colnames(clean_umi_counts)))
print("Percentage of cells filtered out:")
print(paste(perc_filtered_out, "%"))
print("str(clean_umi_counts):")
print(str(clean_umi_counts))

###   Show in plot ranking of cells for NODG, with threshold
# Number of detected genes:
plotname = outdir %&% file_name %&% ".cell_ranking_nodgs.png"
png(plotname, width = 2200, height = 1800, res = 300)
# Plot NODGs ordered by rank (rank-size distribution)
plot (rank(-NODG), NODG,  pch=19,xlab="Cell rank", main = "Cell ranking according to number of detected genes (NODG)", cex.main = 1)
legend("topright",legend = c("threshold for NODG filtering"),
      lty = 1 ,col=c("red"),cex=0.8)
#Threshold cells with low NODG:
abline(threshold_NODG,0,col="red")
dev.off()



print("#####                                  #####")
print("#####          GENE FILTERING          #####")
print("#####                                  #####")

print("Total number of genes before filtering:")
print(length(rownames(clean_umi_counts)))

### Filtering so that only protein-coding genes are kept
print("####################                  Filtering genes out if not protein coding                 #######################")

# Download ensembl data
genomeVersion <- opt$genomeVersion
mart_obj =  useEnsembl(biomart="ensembl", host="www.ensembl.org", dataset="hsapiens_gene_ensembl", GRCh=37)

if (genomeVersion != 'hg19'){
       print('Use genome version GRCh38.')
       mart_obj = useEnsembl(biomart="ensembl", host="www.ensembl.org", dataset="hsapiens_gene_ensembl")
}
print(mart_obj)

entrezGeneMapping_proteinCoding = getBM(attributes= c("ensembl_gene_id", "entrezgene_id", "hgnc_symbol", "description"),
       filters = c("ensembl_gene_id","biotype"), values= list(rownames(umi_counts),'protein_coding'), mart= mart_obj, uniqueRows=T)
print("str(entrezGeneMapping_proteinCoding):")
print(str(entrezGeneMapping_proteinCoding))
print("summary(entrezGeneMapping_proteinCoding):")
print(summary(entrezGeneMapping_proteinCoding))
print("head(entrezGeneMapping_proteinCoding):")
print(head(entrezGeneMapping_proteinCoding))
# get mask if non protein coding filter was applied for each gene, for exporting the information on filtered genes
non_prcoding_mask <- !(rownames(umi_counts) %in% entrezGeneMapping_proteinCoding$ensembl_gene_id)
print("non_prcoding_mask:")
print(head(non_prcoding_mask))
print("length(non_prcoding_mask):")
print(length(non_prcoding_mask))
print("sum(non_prcoding_mask):")
print(sum(non_prcoding_mask))
# see that some ensembl_gene_ids are not unique in the mart object
print("length(entrezGeneMapping_proteinCoding$ensembl_gene_id):")
print(length(entrezGeneMapping_proteinCoding$ensembl_gene_id))
print("length(unique(entrezGeneMapping_proteinCoding$ensembl_gene_id)):")
print(length(unique(entrezGeneMapping_proteinCoding$ensembl_gene_id)))

# filter NAs and multiple entries
# filter NAs
entrezGeneMapping_proteinCoding_noNa = na.omit(entrezGeneMapping_proteinCoding)
print("str(entrezGeneMapping_proteinCoding_noNa):")
print(str(entrezGeneMapping_proteinCoding_noNa))
# get mask if non na filter was applied for each gene, for exporting the information on filtered genes
na_mask <- rownames(umi_counts) %!in% entrezGeneMapping_proteinCoding_noNa$ensembl_gene_id & rownames(umi_counts) %in% entrezGeneMapping_proteinCoding$ensembl_gene_id
print("na_mask:")
print(head(na_mask))
print("length(na_mask):")
print(length(na_mask))
print("sum(na_mask):")
print(sum(na_mask))

# filter duplicated ensembl gene IDs
entrezGeneMapping_proteinCoding_noNa_unique = entrezGeneMapping_proteinCoding_noNa[!duplicated(entrezGeneMapping_proteinCoding_noNa$ensembl_gene_id),]
print("str(entrezGeneMapping_proteinCoding_noNa_unique), after removing duplicate ensembl IDs from mart table:")
print(str(entrezGeneMapping_proteinCoding_noNa_unique))

# filter duplicated hgnc symbols
#entrezGeneMapping_proteinCoding_noNa_unique = entrezGeneMapping_proteinCoding_noNa_unique[!duplicated(entrezGeneMapping_proteinCoding_noNa_unique$hgnc_symbol, incomparables = c("")),]
#entrezGeneMapping_proteinCoding_noNa_unique = entrezGeneMapping_proteinCoding_noNa_unique_IDs[!duplicated(entrezGeneMapping_proteinCoding_noNa_unique_IDs$hgnc_symbol),]
#print("str(entrezGeneMapping_proteinCoding_noNa_unique), after removing duplicate ensembl and after removing duplicate hgnc:")
#print(str(entrezGeneMapping_proteinCoding_noNa_unique))
# get mask if no duplicated hgnc symbol filter was applied for each gene, for exporting the information on filtered genes
#duplicated_hgnc_mask <- rownames(umi_counts) %!in% entrezGeneMapping_proteinCoding_noNa_unique$ensembl_gene_id & rownames(umi_counts) %in% entrezGeneMapping_proteinCoding_noNa_unique_IDs$ensembl_gene_id
#print("duplicated_hgnc_mask:")
#print(head(duplicated_hgnc_mask))
#print("length(duplicated_hgnc_mask):")
#print(length(duplicated_hgnc_mask))
#print("sum(duplicated_hgnc_mask):")
#print(sum(duplicated_hgnc_mask))

print(paste('Mapped to entrez and kept only protein coding genes. Remaining #genes:', dim(entrezGeneMapping_proteinCoding_noNa_unique)[1], sep =' '))
length(entrezGeneMapping_proteinCoding_noNa_unique$ensembl_gene_id)

# creates a boolean (logical) mask vector
res = rownames(clean_umi_counts) %in% entrezGeneMapping_proteinCoding_noNa_unique$ensembl_gene_id

print("Number of genes that are protein coding, do not contain NAs and are no duplicates in ensembl IDs:")
print(sum(res))

# Apply filter for protein coding genes to matrix and gene_id vector
clean_umi_counts=clean_umi_counts[res,]

# Print out numbers
print("Number of genes after protein-coding gene filtering:")
print(length(rownames(clean_umi_counts)))

print("dim(clean_umi_counts):")
print(dim(clean_umi_counts))


###   Filter genes that are absent in all remaining cells
print("####################                  Filtering genes not expressed in remaining cells                 #######################")
# Identify genes absent in all cells:
print("str(clean_umi_counts):")
print(str(clean_umi_counts))
absent_genes <- rowSums(clean_umi_counts) == 0
print("head(absent_genes):")
print(head(absent_genes))
print("str(absent_genes):")
print(str(absent_genes))
print("sum(absent_genes):")
print(sum(absent_genes))
print("str(clean_umi_counts):")
print(str(clean_umi_counts))
names_genes <- row.names(clean_umi_counts)
print("head(names_genes):")
print(head(names_genes))
print("str(names_genes):")
print(str(names_genes))
names_absent_genes <- names_genes[absent_genes]
print("head(names_absent_genes):")
print(head(names_absent_genes))
print("str(names_absent_genes):")
print(str(names_absent_genes))

# remove absent genes from dataset:
clean_umi_counts = clean_umi_counts[!absent_genes,]

# Print out numbers about filtered genes
perc_absent = signif((sum(absent_genes)/length(ENS2HGNC$ensembl_gene_id))*100, digits = 2)
print("Number of genes that are not detected in any of the remaining cells:")
print(sum(absent_genes))
print("Percentage of total number of genes that are not detected in any of the remaining cells:")
print(paste(perc_absent, "%"))
print("Number of genes after absent gene filtering:")
print(length(rownames(clean_umi_counts)))

# get absent gene mask for exporting the information on filtered genes
absent_genes_mask <- rownames(umi_counts) %in% names_absent_genes
print("absent_genes_mask:")
print(head(absent_genes_mask))
print("length(absent_genes_mask):")
print(length(absent_genes_mask))
print("sum(absent_genes_mask):")
print(sum(absent_genes_mask))


###   Filter genes that are MT- genes
print("####################                  Filtering out MT-genes                 #######################")
# Identify MT- genes
mt_genes = rownames(clean_umi_counts) %in% mt

# remove MT- genes from dataset
clean_umi_counts = clean_umi_counts[!mt_genes,]

# Print out numbers about filtered genes
perc_MT = signif((sum(mt_genes)/length(ENS2HGNC$ensembl_gene_id))*100, digits = 2)
print("Number of MT- genes:")
print(sum(mt_genes))
print("Percentage of total number of genes that are MT- genes:")
print(paste(perc_MT, "%"))
print("Number of genes after MT- gene filtering:")
print(length(rownames(clean_umi_counts)))

# get mask of MT genes for exporting the information on filtered genes
MT_mask <- rownames(umi_counts) %in% mt
print("MT_mask:")
print(head(MT_mask))
print("length(MT_mask):")
print(length(MT_mask))
print("sum(MT_mask):")
print(sum(MT_mask))


### Filter genes that encode for ribosomal proteins
print("####################                  Filtering out genes encoding for ribosomal proteins                 #######################")
# get gene_ids of genes encoding for ribosomal proteins
riboall <- as.vector(ENS2HGNC[rownames(clean_umi_counts),1][grep("^(RPL|MRPL|RPS|MRPS)", ENS2HGNC[rownames(clean_umi_counts),2])])
# get gene names
ribo_names <- as.vector(ENS2HGNC[rownames(clean_umi_counts),2][grep("^(RPL|MRPL|RPS|MRPS)", ENS2HGNC[rownames(clean_umi_counts),2])])

# Identify genes encoding for ribosomal proteins:
ribo_genes = rownames(clean_umi_counts) %in% riboall

# remove ribosomal genes from dataset:
clean_umi_counts = clean_umi_counts[!ribo_genes,]

# Print out numbers about filtered genes
perc_ribo = signif((sum(ribo_genes)/length(ENS2HGNC$ensembl_gene_id))*100, digits = 2)
print("Number of ribosomal genes:")
print(sum(ribo_genes))
print("Percentage of total number of genes that are ribosomal genes:")
print(paste(perc_ribo, "%"))
print("Number of genes after ribosomal gene filtering:")
print(length(rownames(clean_umi_counts)))

# get mask of ribosomal proteins for exporting the information on filtered genes
ribo_mask <- rownames(umi_counts) %in% riboall
print("ribo_mask:")
print(head(ribo_mask))
print("length(ribo_mask):")
print(length(ribo_mask))
print("sum(ribo_mask):")
print(sum(ribo_mask))



### Write file with the information on filtered genes
filtered_genes_out <- as.data.frame(rownames(umi_counts))
names(filtered_genes_out) <- "gene_ids"
filtered_genes_out$gene_names <- ENS2HGNC$hgnc_symbol
filtered_genes_out$contains_NAs <- na_mask
#filtered_genes_out$duplicated_hgnc <- duplicated_hgnc_mask
filtered_genes_out$non_protein_coding <- non_prcoding_mask
filtered_genes_out$absent_all_filtered_cells <- absent_genes_mask
filtered_genes_out$MT_genes <- MT_mask
filtered_genes_out$ribosomal_protein_genes <- ribo_mask
print("head(filtered_genes_out):")
print(head(filtered_genes_out))
print("tail(filtered_genes_out):")
print(tail(filtered_genes_out))

txtname = outdir %&% file_name %&% ".information_on_genes_filtered.txt"
write.table(filtered_genes_out, txtname, sep = "\t", row.names = F, col.names = T, quote = F)


# filter cells again in case they do not have counts for any of the remaining genes
print("####################                  Filtering out cells that express none of the remaining genes                 #######################")
cells_filtered_second <- colSums(clean_umi_counts) == 0
print("str(cells_filtered_second):")
print(str(cells_filtered_second))
indic_filtered_second <- which(cells_filtered_second)
print("str(indic_filtered_second):")
print(str(indic_filtered_second))
names_filtered_second <- names(indic_filtered_second)
print("str(names_filtered_second):")
print(str(names_filtered_second))

clean_umi_counts <- clean_umi_counts[,!cells_filtered_second]
print("str(clean_umi_counts)")
print(str(clean_umi_counts))
print("class(clean_umi_counts)")
print(class(clean_umi_counts))
print("Total number of cells before filtering:")
print(length(input_cell_names))
print("Number of cells after second filtering:")
print(length(colnames(clean_umi_counts)))
print("Number of cells filtered out in second round:")
print(length(names_filtered_second))

if (length(names_filtered_second) > 0){
	# Write file with the cell barcodes of the cells that are filtered out
	txtname = outdir %&% file_name %&% ".filtered_cells_second_round.txt"
	write.table(names_filtered_second, txtname, sep = "\t", row.names = F, col.names = F, quote = F)
}

### Show in plot which cells were filtered:
# Log transformed umi counts:
Log_library_size = log2(colSums(umi_counts))
# Point size proportional to library size :
point.size=0.25 + ( Log_library_size -min( Log_library_size  ) )/  diff(range( Log_library_size ) )
# Set a different color for the filtered cells:
col=rep("black",ncol(umi_counts))
col[high_MT]="red"
col[higher_than_threshold]="red"
col[low_NODG]="cyan"
col[lower_than_threshold]="cyan"
col[both_filters]="orange"
col[indic_filtered_second]="magenta"
col[higher_than_threshold & lower_than_threshold]="orange"
col[is_doublet]="limegreen"
color_transparent <- adjustcolor(col, alpha.f = 0.5)
#Plot the fraction of MT reads as a function of the number of detected genes
plotname = outdir %&% file_name %&% ".visualize_filtered_cells.png"
png(plotname, width = 2000, height = 1800, res = 300)
plot(log2(colSums(umi_counts>0)),colSums(umi_counts[mt,])/colSums(umi_counts), pch=19,cex=point.size,col=color_transparent, lwd=0,
     xlab="Log2(Number of Detected Genes)", ylab="Fraction of MT reads",
     main = "Cell filtering based on Number of detected genes and fraction of reads mapping to MT- genes", cex.main = 0.8)
abline(h=threshold_MT, col="red")
abline(v=log2(threshold_NODG), col="cyan")
legend("topright",legend = c("fraction_MT_too_high", "NODG_too_low", "filtered_both_criteria","no genes expressed after gene filtering", "dot size corresponds to library size", "absolute threshold fractionMT","absolute threshold NODG" , "doublets"),
       pch=c(19,19,19,19,19,45,45,19) ,col=c("red","cyan", "orange","magenta", "black", "red", "cyan", "limegreen"),cex=0.8)
dev.off()



######       Export cleaned dataset as hdf5 file        ########

print("Number of gene_names and gene_ids before filtering:")
print(length(ENS2HGNC$hgnc_symbol))
print(length(ENS2HGNC$ensembl_gene_id))

# get filtered gene names that are not linked to matrix as rownames
gene_attrs_filtered <- ENS2HGNC[ENS2HGNC$ensembl_gene_id %in% rownames(clean_umi_counts),]

print("Number of gene_names and gene_ids after filtering:")
print(length(gene_attrs_filtered$hgnc_symbol))
print(head(gene_attrs_filtered$hgnc_symbol))

print(length(gene_attrs_filtered$ensembl_gene_id))
print(head(gene_attrs_filtered$ensembl_gene_id))

# get remaining cell barcodes
output_cell_names <- colnames(clean_umi_counts)
print("Number of cell barcodes after filtering:")
print(length(output_cell_names))

print("str(clean_umi_counts)")
print(str(clean_umi_counts))
print("class(clean_umi_counts)")
print(class(clean_umi_counts))

# add MT fraction to h5 output
fractionMTreads_out <- fractionMTreads[names(fractionMTreads) %in% output_cell_names]
# Make sure MT fractions are in order
stopifnot(all.equal(names(fractionMTreads_out), output_cell_names))
names(fractionMTreads_out) <- c()

# remove colnames and rownames from clean_umi_counts matrix
rownames(clean_umi_counts) <- c()
colnames(clean_umi_counts) <- c()

print("Check if rownames and colnames are NULL")
print(head(rownames(clean_umi_counts)))
print(head(colnames(clean_umi_counts)))

print("dim(clean_umi_counts) after removing row- and colnames")
print(dim(clean_umi_counts))
print("str(clean_umi_counts)")
print(str(clean_umi_counts))
print("class(clean_umi_counts)")
print(class(clean_umi_counts))

# write the output hdf5 file
outfile = outdir %&% file_name_clipped %&% "genes_cells_filtered.h5"
h5createFile(outfile)
h5createGroup(outfile, "cell_attrs")
h5createGroup(outfile, "gene_attrs")
h5write(gene_attrs_filtered$ensembl_gene_id, outfile, "gene_attrs/gene_ids")
h5write(gene_attrs_filtered$hgnc_symbol, outfile, "gene_attrs/gene_names")
h5write(clean_umi_counts, outfile,"raw_counts")
h5write(output_cell_names, outfile, "cell_attrs/cell_names")
h5write(fractionMTreads_out, outfile, "cell_attrs/fractionMT")
