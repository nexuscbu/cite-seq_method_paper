# Cite-Seq methods paper 

This repository includes all scripts for the downstream analysis described in the manuscript titled "Dynamic thresholding and tissue dissociation optimization  for CITE-seq identifies differential surface protein abundance in metastatic melanoma" by Ulrike Lischetti, Aizhan Tastanova et al.


