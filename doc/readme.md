# Readme Hashing / ADT Analysis

0. IndexHopping

1. Analyse Hashing

Goal:  
Get a list with barcodes that are considered to be singlets. In the future the fastqs could be split in this sample also to get back the original samples. (main analysis folder)/hashingAnalysis/[SampleName].barcodes_singlets.txt 

Script: 
hashing_analysis.[Parameters].[SampleName].snake

Tools: 
**CellRanger** ADT --> **CiteSeq** on all Lanes --> **Custom R script** (analyseHashing.R) on CellRanger and CiteSeq output.




2. GEX Pipeline (scTumor Profiler)

ATTENTION:
The config file needs to be adapted to take the barcode list from step Nr. 1 into account.
Under "filter_genes_and_cells":{} the call section needs to point to the custom filter_genes_and_cells.R script. Can be found in the project git.
e.g. "Rscript /cluster/project/nexus/linda/git_eth/levesque_melanoma_2019/scripts/filter_genes_and_cells.R --barcodes_keep /cluster/project/nexus/phrt/levesque_melanoma_2019/data/mel_pilot/Mel_Pilot_10k_GEX_H8/analysis/hashingAnalysis/Mel_Pilot_10k_GEX_H8.barcodes_singlets.txt"

Goal: 
Get Geneexpression for the cells. 

Tools:
scPipeline Git + adapted script **filter_genes_and_cells.R** 



3. Analyse ADT

Custom Script: 
analyseADT.R

4. Ridgeplot across Samples.
plotCombinedRidgeplots.R 

