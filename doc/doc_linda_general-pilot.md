# Levesque Melanoma

Ziel ist vorest die Analyse von sc Pilot Samples auf denen sowohl Feature Barcoding (ADT) als auch Cell hashing getestet wurde. 

Verfügbar sind Momentan 2 Sampels (10k_GEX mit korrespondierendem 10k_ADT, 15kGEX mit 15kADT)

Getestet wurden 
* 4 Hashing Barcodes, wobei Hash 4 jeweils nur in Kombination (ca.15%) mit Hash 1-3 vorkommen sollte. 
* superloading  
* Feature Barcoding (mit zusätzlichen self-conjugated Antibodies)

## Background Infos

Nützliche Analyse Tools:
* Cell Ranger 
* Cite-Seq  https://cite-seq.com/
* Seurat https://satijalab.org/seurat/v3.0/hashing_vignette.html

Nützliche Homepages: 
* https://www.biolegend.com/totalseq (Verwendete Tags: 0251, 0252, 0253, 0254)
* https://support.10xgenomics.com

Experimentelle Hinweise:
Die Hashtags sind nur in den ADT Samples vorhanden, dort liegen sie auf Position 11. Dementsprechend muss Cite-Seq nur auf den ADT Samples laufen. 
In den ADT Samples wurden nebst den "ofiziellen" zusätzliche "self-konjugated" Barcodes genutzt. Eine komplette Liste ist im Ordner metadata zu finden. (feature_reference.txt) 

## Analysen:

### Woche 31:

Rerunning Pipeline Samples on GRCh-38 with the newest pipeline version.

* Pipeline got interupted. Overread that new R packages need to be installed with the newest version. 
* Install R-Packages and restart the run!
* bash auto_run.sh

### Woche 32:
**Montag**
* The 15k Sample went through without issues.
* Can not see a clear problem with the 10k sample. Restarting helped. 

Since the whole analysis is switched to GRCh38 also CiteSeq and CellRanger Count including the ADT Sample needs to be run again to be consistent. 

CellRanger Feature Barcoding has been started with the following two commands. Both Scripts can be found in the scripts folder should however be started from the "final" destination.
> bsub -W 08:00 -R "rusage[mem=4000]" -n 8 bash feature_barcoding_analysis.Mel_Pilot_10k_GEX_H8.sh

> bsub -W 08:00 -R "rusage[mem=4000]" -n 8 bash feature_barcoding_analysis.Mel_Pilot_15k_GEX_H9 


**Dienstag**

It would be interesting to know how many cells there are per hashtag. Proceedings to analyse that:

* Use CellRanger - Filtered feature bc matrix as reference for "valid" barcodes.
* Add up the umi counts over all lanes / per valid barcodes. 
* Count number of cells that contain:
	*  no tags 
	*  ambiguous tags (e.g. more than 20% of the counts assigned to a second tag)
	*  clearly assigned tags.

TO BE DISCUSSED: 
* Does the cutoff of 20% make sense? Or should we go with the seurat classification of singlet/doublet/ambiguous?
Until we are aware off a better cutoff 20% is fine. We should have two sets of results, one based on all cells (raw counts) & one only based on singlets. 

### Woche 33
** Montag **

Look closer again into the Seurat package. 
To find infos about cutoffs etc. 

Found this interesting vignette to analysis ADT data. 
* https://satijalab.org/seurat/v3.0/multimodal_vignette.html
* 
